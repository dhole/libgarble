#include "utils.h"
#include <garble/block.h>

#include <ctype.h>
#include <stdio.h>
#include <time.h>
#include <math.h>

mytime_t
current_time_cycles(void)
{
    unsigned int hi, lo;
    __asm__ __volatile__ ("rdtsc" : "=a"(lo), "=d"(hi));
    return ((unsigned long long) lo) | (((unsigned long long) hi) << 32);
}

mytime_t
current_time_ns(void)
{
    struct timespec tp;
    (void) clock_gettime(CLOCK_MONOTONIC, &tp);
    return 1000000000 * tp.tv_sec + tp.tv_nsec;
}

static int
compare(const void * a, const void * b)
{
    return (*(mytime_t *) a - *(mytime_t *) b);
}

mytime_t
median(mytime_t *values, int n)
{
    qsort(values, n, sizeof(mytime_t), compare);
    if (n == 0)
        return 0;
    else if (n == 1)
        return values[0];
    else if (n % 2 == 1)
        return values[(n + 1) / 2 - 1];
    else
        return (values[n / 2 - 1] + values[n / 2]) / 2;
}

double
doubleMean(double *values, int n)
{
    double total = 0;
    for (int i = 0; i < n; i++)
        total += values[i];
    return total / n;
}

void
print_block(block b)
{
	uint8_t *b8 = (uint8_t *) &b;
	printf("%02x%02x%02x%02x%02x%02x%02x%02x%02x%02x%02x%02x%02x%02x%02x%02x",
	       b8[0], b8[1], b8[2], b8[3], b8[4], b8[5], b8[6], b8[7],
	       b8[8], b8[9], b8[10], b8[11], b8[12], b8[13], b8[14], b8[15]);
}

void
print_bin(bool *str, size_t n)
{
	size_t i;
	for (i = 0; i < n; i++) {
		printf("%d", str[n - 1 - i]);
	}
}

void
u642bin(uint64_t v, bool *str, size_t n)
{
	size_t i;
	for (i = 0; i < n; i++) {
		str[i] = v & (1U << i);
	}
	return;
}

uint64_t
bin2u64(bool *str, size_t n)
{
	size_t i;
	uint64_t v = 0;
	for (i = 0; i < n; i++) {
		 v += str[i] * ((uint64_t) pow(2, i));
	}
	return v;
}

bool
checkCircuit(garble_circuit *gc, block *inputLabels,
             void check(bool *a, bool *out, int s)) {

	int i, j;
	int n = gc->n;
	int m = gc->m;
	block *extractedLabels;
	block computedOutputMap[m];
	bool outputReal[m];
        bool outputVals[m];
	bool *inputs;
        bool res = true;

	extractedLabels = garble_allocate_blocks(n);
	inputs = calloc(n, sizeof(bool));

	for (i = 0; i < TIMES; i++) {
		//printf("\nInput: ");
		for (j = 0; j < n; j++) {
			inputs[j] = rand() % 2;
		}
		//print_bin(&inputs[0], n/2);
		//printf(", ");
		//print_bin(&inputs[n/2], n/2);
		//printf("\n");
                garble_extract_labels(extractedLabels, inputLabels, inputs, gc->n);
                garble_eval(gc, extractedLabels, computedOutputMap, outputVals);

		check(inputs, outputReal, n);
		//printf("Vals: %lu = ", bin2u64(outputVals, 32));
		//print_bin(outputVals, 32);
		//printf("\n");
		for (j = 0; j < m; j++) {
			if (outputVals[j] != outputReal[j]) {
				fprintf(stderr,
					"Check failed: in = (%lu, %lu) "
					"got %lu, expected %lu\n",
					bin2u64(inputs, n/2), bin2u64(inputs + n/2, n/2),
					bin2u64(outputVals, m), bin2u64(outputReal, m));
                                res = false;
				break;
			}
		}
		if (j == m) {
		    fprintf(stderr,
			    "Check passed: in = (%lu, %lu) "
			    "got %lu, expected %lu\n",
			    bin2u64(inputs, n/2), bin2u64(inputs + n/2, n/2),
			    bin2u64(outputVals, m), bin2u64(outputReal, m));
		}
	}
	free(inputs);
	free(extractedLabels);
	return res;
}

