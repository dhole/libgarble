#include "garble.h"
#include "garble/block.h"
#include "circuits.h"

#include "utils.h"

#include <assert.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <stdio.h>
#include <time.h>

#include <openssl/sha.h>

size_t _b = 16;
size_t _k = 1000;
size_t _n;
size_t _m;

static void
sum_fn(bool *inputs, bool *outputs, int n) {
    size_t i;
    uint64_t in[_k], out = 0;
    //printf("Real: sum(");
    for (i = 0; i < _k; i++) {
        in[i] = bin2u64(&inputs[i * n/_k], n/_k);
        //printf("%lu, ", in[i]);
    }
    for (i = 0; i < _k; i++) {
        out += in[i];
    }
    //printf(") = %lu = ", out);
    u642bin(out, outputs, n/_k);
    //print_bin(outputs, n/_k);
    //printf("\n");
    return;
}

static void
build(garble_circuit *gc, garble_type_e type)
{
    garble_context ctxt;

    int *inputs = malloc(_n * sizeof(int));
    int *outputs = malloc(_m * sizeof(int));

    garble_new(gc, _n, _m, type);
    builder_start_building(gc, &ctxt);
    builder_init_wires(inputs, _n);

    fold(gc, &ctxt, inputs, outputs, _k, _b, circuit_add_NN);
    //circuit_add(gc, &ctxt, b * 2, inputs, outputs, NULL);
    builder_finish_building(gc, &ctxt, outputs);
}

static int
run(garble_type_e type)
{
    size_t m = _m, n = _n;
    const int times = 1,
              niterations = 1;
    garble_circuit gc;

    block *inputLabels = garble_allocate_blocks(2 * _n);
    block *extractedLabels = garble_allocate_blocks(_n);
    block *outputMap = garble_allocate_blocks(2 * _m);
    bool *inputs = calloc(_n, sizeof(bool));
    block seed;

    unsigned char hash[SHA_DIGEST_LENGTH];

    printf("Type: ");
    switch (type) {
    case GARBLE_TYPE_STANDARD:
        printf("Standard\n");
        break;
    case GARBLE_TYPE_HALFGATES:
        printf("Half-gates\n");
        break;
    case GARBLE_TYPE_PRIVACY_FREE:
        printf("Privacy free\n");
        break;
    }

    build(&gc, type);

    seed = garble_seed(NULL);

    if (garble_garble(&gc, NULL, outputMap) == GARBLE_ERR) {
        fprintf(stderr, "garble failed\n");
        return 1;
    }

    // Fill inputLabels with the input wire mapping from gc
    memcpy(inputLabels, gc.wires, 2 * gc.n * sizeof(block));
    garble_hash(&gc, hash);

    {
        block *computedOutputMap = garble_allocate_blocks(m);
        bool *outputVals = calloc(m, sizeof(bool));
        bool *outputVals2 = calloc(m, sizeof(bool));
        for (size_t i = 0; i < n; ++i) {
            inputs[i] = rand() % 2;
        }
        // Map inputs to extractedLabels using the mapping inputLabels
        garble_extract_labels(extractedLabels, inputLabels, inputs, gc.n);
        garble_eval(&gc, extractedLabels, computedOutputMap, outputVals);
        assert(garble_map_outputs(outputMap, computedOutputMap, outputVals2, m) == GARBLE_OK);
        // Verify that the mapping gives the same results as the evaluation,
        // which uses the output_perms to get the circuit result.
        for (uint64_t i = 0; i < gc.m; ++i) {
            assert(outputVals[i] == outputVals2[i]);
        }
        {
            // Check that the circuit behaves according to the test
            if (checkCircuit(&gc, inputLabels, sum_fn)) {
                printf("Circuit check passed!\n");
            }
        }
        {
            // Generate the same garbled circuit again and verify that it's the
            // same as before.
            garble_circuit gc2;

            (void) garble_seed(&seed);
            build(&gc2, type);
            garble_garble(&gc2, NULL, NULL);
            assert(garble_check(&gc2, hash) == GARBLE_OK);
            garble_delete(&gc2);
        }

        {
            // Write the garbled circuit into a file, read it back, evaluate it
            // and verify that it works.
            FILE *f;
            f = fopen("sum.gc", "w");
            garble_save(&gc, f, true, false);
            fclose(f);
            garble_delete(&gc);

            build(&gc, type);
            f = fopen("sum.gc", "r");
            garble_load(&gc, f, true, false);
            fclose(f);

            garble_eval(&gc, extractedLabels, computedOutputMap, outputVals);
            garble_delete(&gc);

            assert(garble_map_outputs(outputMap, computedOutputMap, outputVals2, m) == GARBLE_OK);
            for (uint64_t i = 0; i < gc.m; ++i) {
                assert(outputVals[i] == outputVals2[i]);
            }

        }

        free(computedOutputMap);
        free(outputVals);
        free(outputVals2);
    }

    {
        // Time the evaluation in cycles per gate
        mytime_t start, end;
        double garblingTime, evalTime;
        mytime_t *timeGarble = calloc(times, sizeof(mytime_t));
        mytime_t *timeEval = calloc(times, sizeof(mytime_t));
        double *timeGarbleMedians = calloc(times, sizeof(double));
        double *timeEvalMedians = calloc(times, sizeof(double));
        bool *outputs = calloc(m, sizeof(bool));

        build(&gc, type);

        for (int j = 0; j < times; j++) {
            for (int i = 0; i < times; i++) {
                start = current_time_cycles();
                {
                    (void) garble_garble(&gc, inputLabels, outputMap);
                }
                end = current_time_cycles();
                timeGarble[i] = end - start;

                start = current_time_cycles();
                {
                    garble_extract_labels(extractedLabels, inputLabels, inputs, gc.n);
                    garble_eval(&gc, extractedLabels, NULL, outputs);
                }
                end = current_time_cycles();
                timeEval[i] = end - start;
            }
            timeGarbleMedians[j] = ((double) median(timeGarble, times)) / gc.q;
            timeEvalMedians[j] = ((double) median(timeEval, times)) / gc.q;
        }
        garblingTime = doubleMean(timeGarbleMedians, times);
        evalTime = doubleMean(timeEvalMedians, times);
        printf("%lf %lf\n", garblingTime, evalTime);

        garble_delete(&gc);

        free(timeGarble);
        free(timeEval);
        free(timeGarbleMedians);
        free(timeEvalMedians);
        free(outputs);
    }

    {
        // Time the evaluation in seconds
        unsigned long long start, end;
        bool *outputs = calloc(m, sizeof(bool));

        build(&gc, type);

        start = current_time_ns();
        for (int i = 0; i < niterations; ++i) {
            (void) garble_garble(&gc, inputLabels, outputMap);
            garble_extract_labels(extractedLabels, inputLabels, inputs, gc.n);
            garble_eval(&gc, extractedLabels, NULL, outputs);
        }
        end = current_time_ns();
        printf("t = %f s\n", (end - start) / 1000000000.0);

        garble_delete(&gc);

        free(outputs);
    }


    free(inputs);
    free(extractedLabels);
    free(outputMap);
    free(inputLabels);

    return 0;
}

int
main(int argc, char *argv[])
{
    garble_type_e type;
    bool set_type = false;
    if (argc >= 3) {
        _k = atoi(argv[1]);
        _b = atoi(argv[2]);
        if (_b > 64) {
            printf("_b shouldn't be bigger than 64\n");
            return 1;
        }
        if (argc == 4) {
            if (strcmp(argv[3], "standard") == 0) {
                type = GARBLE_TYPE_STANDARD;
            } else if (strcmp(argv[3], "halfgates") == 0) {
                type = GARBLE_TYPE_HALFGATES;
            } else if (strcmp(argv[3], "privacyfree") == 0) {
                type = GARBLE_TYPE_PRIVACY_FREE;
            } else {
                return 1;
            }
            set_type = true;
        }
    }
    _n = _k * _b;
    _m = _b;

    if (set_type) {
        if (run(type))
            return 1;
    } else {
        if (run(GARBLE_TYPE_STANDARD))
            return 1;
        if (run(GARBLE_TYPE_HALFGATES))
            return 1;
        if (run(GARBLE_TYPE_PRIVACY_FREE))
            return 1;
    }

    return 0;
}
