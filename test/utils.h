#ifndef LIBGARBLE_TEST_UTILS_H
#define LIBGARBLE_TEST_UTILS_H

#include "garble.h"
#include "garble/block.h"

#include <stdint.h>
#include <stdbool.h>

#define TIMES 32

typedef unsigned long long mytime_t;

mytime_t
current_time_cycles(void);
mytime_t
current_time_ns(void);


mytime_t
median(mytime_t *values, int n);
double
doubleMean(double A[], int n);

void print_block(block b);
void print_bin(bool *str, size_t n);
void u642bin(uint64_t v, bool *str, size_t n);
uint64_t bin2u64(bool *str, size_t n);
bool checkCircuit(garble_circuit *gc, block *inputLabels,
             void check(bool *a, bool *out, int s));
#endif
