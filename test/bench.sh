#! /bin/sh
#set -x

MODE=$1
TESTS=$2

cd test/
run_ben() {
  #INPUTS="5 10 50 100 500 1000 5000 10000"
  TYPES="standard halfgates privacyfree"
  INPUTS="2 4 8 16 32 64 128 256 512 1024 2048 4096"
  BITS="16 32"

  echo "Run"
  TESTS=$1
  if [ "$TESTS" = "" ]
  then
    TESTS="sum min"
  fi

  for TEST in $TESTS
  do
    rm -f "$TEST"_time.txt
    rm -f "$TEST"_size.txt

    echo "bits inputs $TYPES" > "$TEST"_time.txt
    echo "bits inputs $TYPES" > "$TEST"_size.txt
    for B in $BITS
    do
      for INPUT in $INPUTS
      do
	echo "$TEST $B $INPUT..."
	TIMES="$B $INPUT"
	SIZES="$B $INPUT"
	for TYPE in $TYPES
	do
	  RES=`./$TEST $INPUT $B $TYPE | grep -Po "(?<=t = )[0-9\.].*(?= s)"`
	  TIMES="$TIMES $RES"
	  SIZES="$SIZES `wc -c < $TEST.gc`"
	done
	echo "$TIMES" >> "$TEST"_time.txt
	echo "$SIZES" >> "$TEST"_size.txt
      done
    done
  done
}

plot() {
  echo "Plot"
}

if [ "$MODE" = "run" ]
then
  run_ben $TESTS
elif [ "$MODE" = "plot" ]
then
  plot
else
  run $TESTS
  plot
fi
