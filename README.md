# libgarble
Garbling library based on [JustGarble](http://cseweb.ucsd.edu/groups/justgarble/).

This code is still in alpha and the API is not yet fully stable (as in, future commits may change it).  However, I'd love to hear feedback from anyone who uses it, to help improve the library.

## Installation instructions

Run the following:
```
autoreconf -i
./configure
make
sudo make install
```
This installs two libraries: `libgarble` for actually garbling and evaluating a circuit, and `libgarblec` for writing circuits.

To test, run the following:
```
cd test
make aes
./aes
```

To compile with debug flags:
```
./configure --enable-debug
make clean
make
```

This will garble and evaluate an AES circuit, and present timings in cycles/gate.

# History

- [JustGarble](http://cseweb.ucsd.edu/groups/justgarble/) is made by Mihir
  Bellare, Viet Tung Hoang, Sriram Keelveedhi and Phillip Rogaway, and is
  published under the GNU GPL v3.0 License.
- [libgarble](https://github.com/amaloz/libgarble) is an adaptation of
  JustGarble made by Alex J. Malozemoff
- This is a fork of libgarble made by Bhaskar Krishnamachari, Muhammad Naveed,
  Eduard Sanou, Kwame-Lante Wright and is published under the GNU GPL v3.0
  License.

# Changelog

The changes to libgarble made in this fork are:

- Add LICENSE
- Implement OR and NOT gates for `GARBLE_TYPE_STANDARD`.
- `builder/circuits.c`
    - Comment circuits functionality
    - Add `circuit_add_NN()`: Add 2 N-bit integers, output N bits
    - Add `circuit_mul_fxp()`: Multiply 2 N-bit fixed point numbers, output 2N bits
    - Add `circuit_mul_fxp_NN()`: Multiply 2 N-bit fixed point numbers, output N bits
    - Add `circuit_sign_ext()`: Copy input to output with sign extension
    - Add `circuit_neg`: Negate an N-bit signed number
    - Add `circuit_abs`: Return absolute value from a signed N bit number
    - Add `circuit_max`: Return the max signed value out of 2 N-bit numbers
    - Add `circuit_div_fxp()`: Divide 2 N-bit fixed point numbers, output N bits
    - Add `fold()`sum
- `test/`
    - `bench.sh`: Add benchmarking script 
    - `min.c`: Add example to evaluate the min over k integers
    - `sum.c`: Add example to evaluate the sum over k integers
    - `fixed_mult.c`: Add example to evaluate the fixed point multiplication over k integers
    - `utils.c`
        - Add `print_block()`
        - Add `print_bin()`
        - Add `u642bin()`
        - Add `bin2u64()`
        - Add `checkCircuit()`
